var replaceContents = require('../src'),
    path    = require('path'),
    fs      = require('fs'),
    vinylFs = require('vinyl-fs'),
    through = require('through2'),
    assert = require('power-assert');


describe('stream-replace-content', function() {
    var file = path.join(__dirname, 'test.txt');

    xit('は第1引数が文字列でも正規表現でもない場合はerrorをemitして停止する', function() { });
    xit('は第2引数が文字列でも関数でもない場合はerrorをemitして停止する', function() {});

    describe('ストリームの場合', function() {
        beforeEach(function(done){
            fs.writeFileSync(file, 'hogehoge');
            done();
        });
        afterEach(function(done) {
            fs.unlinkSync(file);
            done();
        });

        it('文字 => 文字の場合', function() {
            fs.createReadStream(file)
                .pipe(replaceContents('hoge', 'fuga'))
                .pipe(through.obj(function(chunk, enc, cb) {
                    assert(chunk.toString('utf8') == 'fugahoge');
                    this.push(chunk);
                    cb();
                }))
                .pipe(process.stderr)
        });
        it('文字 => 関数の場合', function() {
            fs.createReadStream(file)
                .pipe(replaceContents('hoge', function(m) { return '['+ m +']'}))
                .pipe(through.obj(function(chunk, enc, cb) {
                    assert(chunk.toString('utf8') == '[hoge]hoge');
                    this.push(chunk);
                    cb();
                }))
        });
        it('正規表現 => 文字の場合', function() {
            fs.createReadStream(file)
                .pipe(replaceContents(/oge/g, 'uga'))
                .pipe(through.obj(function(chunk, enc, cb) {
                    assert(chunk.toString('utf8') == 'hugahuga');
                    this.push(chunk);
                    cb();
                }))
        });
        it('正規表現 => 関数の場合', function() {
            fs.createReadStream(file)
                .pipe(replaceContents(/(og)/g, function(m, m1) { return '['+ m1 +']'; }))
                .pipe(through.obj(function(chunk, enc, cb) {
                    assert(chunk.toString('utf8') == 'h[og]eh[og]e');
                    this.push(chunk);
                    cb();
                }))
        });
    });
    describe('vinylファイルの場合', function() {
        beforeEach(function(done){
            fs.writeFileSync(file, 'hogehoge');
            done();
        });
        afterEach(function(done) {
            fs.unlinkSync(file);
            done();
        });

        it('文字 => 文字の場合', function(done) {
            vinylFs.src(file)
                .pipe(replaceContents('hoge', 'fuga'))
                .pipe(through.obj(function(file, enc, cb) {
                    assert(file.contents.toString('utf8') == 'fugahoge');
                    this.push(file);
                    cb();
                }))
                .pipe(vinylFs.dest(__dirname))
                .on('end', done)
        });
        it('文字 => 関数の場合', function(done) {
            vinylFs.src(file)
                .pipe(replaceContents('hoge', function(m) { return '['+ m +']'}))
                .pipe(through.obj(function(file, enc, cb) {
                    assert(file.contents.toString('utf8') == '[hoge]hoge');
                    this.push(file);
                    cb();
                }))
                .pipe(vinylFs.dest(__dirname))
                .on('end', done)
        });
        it('正規表現 => 文字の場合', function(done) {
            vinylFs.src(file)
                .pipe(replaceContents(/oge/g, 'uga'))
                .pipe(through.obj(function(file, enc, cb) {
                    assert(file.contents.toString('utf8') == 'hugahuga');
                    this.push(file);
                    cb();
                }))
                .pipe(vinylFs.dest(__dirname))
                .on('end', done)
        });
        it('正規表現 => 関数の場合', function(done) {
            vinylFs.src(file)
                .pipe(replaceContents(/(og)/g, function(m, m1) { return '['+ m1 +']'; }))
                .pipe(through.obj(function(file, enc, cb) {
                    assert(file.contents.toString('utf8') == 'h[og]eh[og]e');
                    this.push(file);
                    cb();
                }))
                .pipe(vinylFs.dest(__dirname))
                .on('end', done)
        });

    });
});
