var through = require('through2');

module.exports = function(pattern, newSubStr) {
    return through.obj(function(chunk, enc, callback) {
        try {
            var data;
            if (chunk.contents) { // maybe vinyl-fs
                data = chunk.contents;
            } else {
                data = chunk;
            }
            var newChunk = new Buffer(data.toString('utf8').replace(pattern, newSubStr));
            if (chunk.contents) {
                chunk.contents = newChunk;
            } else {
                chunk = newChunk;
            }
            this.push(chunk);
            callback();
        } catch(e) {
            this.emit("error", e);
        }
    });
};

